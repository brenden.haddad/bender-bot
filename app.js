
// Call all package you install.
const Discord = require ('discord.js');
const bot = new Discord.Client();
const weather = require('weather-js');
const fs = require('fs');

// Call JSON files here
const commands = JSON.parse(fs.readFileSync('Storage/commands.json', 'utf8'));

// Bot Settings - Global settings
const prefix = '++'; // Prefix for commands.

// Functions
function hook(channel, title, message, color, avatar) { // Last two are optional.
  // Reassign default parameters - If any are blank.
  if (!channel) return console.log('Channel not specified.');
  if (!title) return console.log('Title not specified.');
  if (!message) return console.log('Message not specified.');
  if (!color) color = 'd9a744'; // This is optional. Choose your HEX color.
  if (!avatar) avatar = 'https://cdn0.iconfinder.com/data/icons/thin-communication-messaging/24/thin-0306_chat_message_discussion_bubble_conversation-512.png' // This is optional. Choose your own Icon if you want.

  // Removing spaces from color and avatarURL.
  color = color.replace(/\s/g, '');
  avatar = avatar.replace(/\s/g, '');

  // Creating Webhook.
  channel.fetchWebhooks() // Gets webhook in channel.
    .then(webhook => {

      // Fetches webhook we will use for each hook.
      let foundHook = webhook.find('name', 'Webhook');

      // This runs if the webhook is not found.
      if (!foundHook) {
        channel.createWebhook('Webhook', 'https://cdn0.iconfinder.com/data/icons/thin-communication-messaging/24/thin-0306_chat_message_discussion_bubble_conversation-512.png')
          .then(webhook => {
            // Send the Webhook
            webhook.send('', {
              "username": title,
              "avatarURL": avatar,
              "embeds": [{
                "color": parseInt(`0x${color}`),
                "description": message
              }]
            })
              .catch(error => { // If error is found, report in chat.
                console.log(error)
                return channel.send('**Something went wrong when sending the webhook. Please check console.**')
              })
          })
      } else {
          foundHook.send('', {
            "username": title,
            "avatarURL": avatar,
            "embeds": [{
              "color": parseInt(`0x${color}`),
              "description": message
            }]
          })
            .catch(error => { // If error is found, report in chat.
              console.log(error)
              return channel.send('**Something went wrong when sending the webhook. Please check console.**')
            })
      }

    })
}

// Listener Event: Runs whenver a message is received.
bot.on('message', message => {

  // Variables - Variables to call things, makes it easier and less typing
  let msg = message.content.toLowerCase(); // Turns variable into all lowercase.
  let sender = message.author; // Takes the message and finds who the author is.
  let cont = message.content.slice(prefix.length).split(" "); // Slices off prefix, then puts the rest in an array.
  let args = cont.slice(1); // Slices off the command in cont, only leaving the arguments

  // Commands - Useful commands
  if (msg === prefix + 'ping') {
      message.channel.send('pong!');// sends response as 'Ping'
  }

  // PURGE Command
  if (msg.startsWith(prefix + 'purge')){
    async function purge() {
      message.delete(); // Deletes the command message, so it doesn't interfere with messages we will delete

      if (!message.member.roles.find("name", "bender")){ // Checks to see if bot-commander role is applied. ! inverts the true/false.
        message.channel.send('You need the \ `bender\` role to use this command.'); // Tells user in chat that they need the role.
        return;
      }

      // Check if argument is a number
      if (isNaN(args[0])) {
        // Sends message to channel
        message.channel.send('Please use a number as your arguments. \n Usage: ' + prefix + 'purge <amount>');
        // Cancels the script, so rest doesn't run.
        return;
      }

      const fetched = await message.channel.fetchMessages({limit: args[0]}); // Grabs the last number of messages in the channel
      console.log(fetched.size + ' messages found, deleting...'); // Posts how many messages we are deleting

      // Deleting of messages
      message.channel.bulkDelete(fetched)
        .catch(error => message.channel.send(`Error: ${error}`)); // Finds an error and posts it into the channel

    }

    // Call the function whenever the purge command is called
    purge(); // Must be inside the if (msg.startsWith)
  }

  // Weather Command
  if (msg.startsWith(prefix + 'weather')) {

    weather.find({search: args.join(" "), degreeType: 'F'}, function(err, result) {
      if (err) message.channel.send(err);

      // Invalid info was provided
      if (result.length === 0) {
        message.channel.send('**Error!**\nPlease enter a valid location!') // Error message saying in chat that the place they entered is invalid.
        return;
      }

      // Variables
      var current = result[0].current; // Variable for the current part of JSON output.
      var location = result[0].location; // Variable for the location part of the JSON output.

      // Embed version.
      const embed = new Discord.RichEmbed()
        .setDescription(`**${current.skytext}**`) // Text of what the sky looks like.
        .setAuthor(`Weather for ${current.observationpoint}`) // Current location of weather.
        .setThumbnail(current.imageUrl) // Sets the thumbnail of the embed
        .setColor(0x00AE86) // Sets the color of the emgbed, 0x must e front of the hex code.
        .addField('Timezone',`UTC${location.timezone}`, true) // First field that shows timezone
        .addField('Degree Type',location.degreetype, true) // Shows degree type
        .addField('Temperature',`${current.temperature} Degrees`, true)
        .addField('Feels Like',`${current.feelslike} Degrees`, true)
        .addField('Winds',current.winddisplay, true)
        .addField('Humidity',`${current.humidity}%`, true)

      // Sends the rough version of the current weather
      message.channel.send({embed})
    });
  }

  // Webhooks
  if (msg.startsWith(prefix + 'hook')) { // Startswith because command has arguments.

      // Delete the message that the user Sends
      message.delete();

      if (msg === prefix + 'hook') {
        return hook(message.channel, 'Hook Usage', `${prefix}hook <title>, <message>, [HEXcolor], [avatarURL]\n\n**<> is required\n[] is optional**`,'FC8469','https://cdn0.iconfinder.com/data/icons/thin-communication-messaging/24/thin-0306_chat_message_discussion_bubble_conversation-512.png')
      }

      let hookArgs = message.content.slice(prefix.length + 4).split(","); // Slices first six letters (prefix & word hook) then splits them by commands

      hook (message.channel, hookArgs[0], hookArgs[1], hookArgs[2], hookArgs[3]); // Calls the hook here
  }

  // Help command
  if (msg.startsWith(prefix + 'help')) {

    // For only !help
    if (msg === `${prefix}help`) { // For regular users

      const embed = new Discord.RichEmbed()
        .setColor(0x1D82B6)

      // Variables
      let commandsFound = 0; // How many commands are found for that group

      // Loop that loops through the commands
      for (var cmd in commands) {

        // Checks if group is users
        if (commands[cmd].group.toLowerCase() === 'user') {
          // count commands
          commandsFound++

          embed.addField(`${commands[cmd].name}`, `**Description:** ${commands[cmd].desc}\n**Usage:** ${prefix + commands[cmd].usage}`);
        }
      }
      // More embeds
      embed.setFooter(`Currently showing user commands. To view another group do ${prefix}help [group / command]`)
      embed.setDescription(`**${commandsFound} commands found** - <> means required, [] means optional`)

      // Sending to DMs
      message.author.send({embed})
      // Post in chat they sent to DMs
      message.channel.send({embed: {
        color: 0x1D82B6,
        description: `**Check your DMs ${message.author}!**`,
      }})

    } else if (args.join(" ").toLowerCase() === 'groups') {
      //Variables
      let groups = '';

      for (var cmd in commands) {
        if (!groups.includes(commands[cmd].group)) {
          groups += `${commands[cmd].group}\n`
        }
      }

      message.channel.send({embed: {
        description: `**${groups}**`,
        title:"Groups",
        color: 0x1D82B6,
      }})

      return;

    }else {

        //Variables
        let groupFound= "";

        for (var cmd in commands) {

          if (args.join(" ").trim().toLowerCase() === commands[cmd].group.toLowerCase()) {
            groupFound = commands[cmd].group.toLowerCase(); // Set group found then break out of loops
            break;
          }
        }

        if (groupFound != '') {

          const embed = new Discord.RichEmbed()
            .setColor(0x1D82B6)

          // Variables
          let commandsFound = 0; // How many commands are found for that group

          for (var cmd in commands) {

            // Checks if group is users
            if (commands[cmd].group.toLowerCase() === groupFound) {
              // count commands
              commandsFound++

              embed.addField(`${commands[cmd].name}`, `**Description:** ${commands[cmd].desc}\n**Usage:** ${prefix + commands[cmd].usage}`);
            }
          }
          // More embeds
          embed.setFooter(`Currently showing ${groupFound} commands. To view another group do ${prefix}help [group / command]`)
          embed.setDescription(`**${commandsFound} commands found** - <> means required, [] means optional`)

          // Sending to DMs
          message.author.send({embed})
          // Post in chat they sent to DMs
          message.channel.send({embed: {
            color: 0x1D82B6,
            description: `**Check your DMs ${message.author}!**`
          }})

          return; // We want to make sure we trun so it doesn't run the rest of the script after it finds a group

        }

        // Variables
        let commandFound = '';
        let commandDesc = '';
        let commandUsage = '';
        let commandGroup = '';

        // If group is not found
        for (var cmd in commands) {

          if (args.join(" ").trim().toLowerCase() === commands[cmd].name.toLowerCase()) {
            commandFound = commands[cmd].name.toLowerCase();
            commandDesc = commands[cmd].desc.toLowerCase();
            commandUsage = commands[cmd].usage.toLowerCase();
            commandGroup = commands[cmd].group.toLowerCase();
            break;
        }
      }
      // Lets post in chat if nothing is found
      if (commandFound === '') {
        message.channel.send({embed: {
          description: `**No group or command found titled \`${args.join(" ")}\`**`,
          color: 0x1D82B6,
        }})
      }

      // Post in chat they sent to DMs
      message.channel.send({embed: {
        title: '<> means required, [] means optional',
        color: 0x1D82B6,
        fields: [{
          name: commandFound,
          value: `**Description:** ${commandDesc}\n**Usage:** ${commandUsage}\n **Group:** ${commandGroup}`
        }]
      }})
    }
  }

  // Care command
  if (msg.startsWith(prefix + 'care')) {

    // For only !help
    if (msg === `${prefix}care`) { // For regular users

      const embed = new Discord.RichEmbed()
        .setColor(0x1D82B6)

      // Variables
      let embed1 = new Discord.RichEmbed( {
        title: '**Welcome to Radiant Journey!**',
        image: {
          url: 'https://media.discordapp.net/attachments/572942877521477633/585519400203976713/RadiantJourney_Logo.png?width=1442&height=306'
        }
      });

      // Loop that loops through the commands
      for (var cmd in commands) {

        // Checks if group is users
        if (commands[cmd].group.toLowerCase() === 'user') {
          //embed.setTitle(`**Welcome to Radiant Journey!**`);
          embed.addField(`${commands[cmd].name}`, `**Description:** ${commands[cmd].desc}\n**Usage:** ${prefix + commands[cmd].usage}`);
        }
      }
      // More embeds
      embed.setFooter(`Currently showing user commands. To view another group do ${prefix}help [group / command]`)
      embed.setDescription(``)

      // Sending to DMs
      message.author.send({embed: {
        title: '**Welcome to Radiant Journey!**',
        image: {
          url: 'https://media.discordapp.net/attachments/572942877521477633/585519400203976713/RadiantJourney_Logo.png?width=1442&height=306',
        },
        //description: "We are so glad to have you with us!\n\n\nThe *first thing* you probably want is an invite into the Supergroup. Well, here are a couple ways to go about that:\n**1.** Join the official Radiant Journey global chat channel. You can do this using the in-game chat, by typing: **/chan_join radiantjourney.**\nThen, ask in that channel if anyone is online that can send an invite. This channel is global, meaning you will able to chat to any person cross-servers and cross-alignment.\n**2.** If no one responds, then ask the **@Gatekeeper Invites** role in our Discord and someone will hop into the game as soon as possible to get you invite.\n\n\nFeel free to post something in the **#introductions** channel and tell us a little about yourself. We'd love to get to know you better!\nIf you have any questions, please ask. We already have an amazing, helpful group of people here, so take advantage!\n\n\nRadiant Journey is based on Excelsior with branches on all the other servers, additionally we have a group on the Rebirth (i24) servers if you main there.\nOn Excelsior we have 4 SG branches hero side and 1 on villain side.\nHere are all of our SG names, if one is full we will send an invite from one of the other ones, all linked with coalition chat for easy communication!\n\n`Radiant Journey (Excelsior & Rebirth)`\n`Radiant Voyage (Excelsior & Rebirth)`\n`Radiant Destiny (Excelsior)`\n`Radiant Stars (Excelsior)`\n`Smouldering Journey (Excelsior & Rebirth)`\n\n\nTo be eligible to get the rank of **Idealist** in-game (and on Discord) please visit our **#start-here** channel to get the **Explorer** role by interacting with our rules that you agree.\nYou can also add your character to the roster on the wiki and explore a little bit on our website <https://www.radiant-journey.com/>",

      }})
      // Post in chat they sent to DMs
      message.channel.send({embed: {
        color: 0x1D82B6,
        description: `**Package delivered**... ${message.author}!`,
      }})
    }
    }
});

// Lisenter Event: Runs whenever the bot sends a ready event (when it first starts)
bot.on('ready', () => {
  // Bot has started.
  console.log('Bot has started.')
});

// Login now
bot.login('NTg4NTE3MjMyNzcwODA5ODY4.XQbeng.eRto0QC8Rc70eQeUQ4E0mzsTRek');
